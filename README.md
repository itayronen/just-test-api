# just-test-api

## Install
`npm install --save-dev just-test-api`

## Usage
Export a default function that accepts a test suite.  
To fail a test just `throw`, or use an assertion library.  

```ts
import { TestSuite, TestParams } from "just-test-api";
import { expect } from "chai"; // Example of an assertion library.
import { MyClass } from "./MyClass"; // The class you want to test.

export default function(suite: TestSuite) {
	suite.describe("MyClass", (suite: TestSuite) => {
		suite.test("some test", () => {
			let myClass = new MyClass();

			let actual = myClass.foo();

			expect(actual).to.equal("hello");
		});

		suite.test("some test with stages", (t: TestParams) => {
			t.arrange();
			let myClass = new MyClass();

			t.act();
			let actual = myClass.foo();

			t.assert();
			expect(actual).to.equal("hello");
		});

		suite.test("async test", async (t: TestParams) => {
			let myClass = new MyClass();

			let actual = await myClass.foo();

			expect(actual).to.equal("hello");
		});

		suite.testFunc(when_adding_2_number_then_the_sum_is_correct);
		
		suite.testFunc(function inline_function_with_name(t: TestParams) {
			expect(1 + 2).to.equal(3);
		});

		suite.testFuncs(test1, test2);
	});
}

function when_adding_2_number_then_the_sum_is_correct(t: TestParams) {
	expect(1 + 2).to.equal(3);
}

function test1(t: TestParams) {}
function test2(t: TestParams) {}
```