export interface TestSuite {
	describe(description: string | { name: string }, suiteFunction: (suite: TestSuite) => void): void;
	test(description: string, testFunction: (test: TestParams) => Promise<void> | void): void;
	testFunc(namedTestFunction: (test: TestParams) => Promise<void> | void): void;
}

export interface TestParams {
	arrange(): void;
	act(): void;
	assert(): void;

	timeout?: number;
}